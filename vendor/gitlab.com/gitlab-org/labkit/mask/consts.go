package mask

// RedactionString represents the filtered value used in place of sensitive data in the mask package
const RedactionString = "[FILTERED]"
