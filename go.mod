module gitlab.com/gitlab-org/gitaly

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cloudflare/tableflip v1.2.1-0.20200514155827-4baec9811f2b
	github.com/getsentry/sentry-go v0.5.1
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/lib/pq v1.2.0
	github.com/libgit2/git2go/v30 v30.0.5
	github.com/olekukonko/tablewriter v0.0.2
	github.com/opentracing/opentracing-go v1.0.2
	github.com/prometheus/client_golang v1.0.0
	github.com/prometheus/procfs v0.0.3 // indirect
	github.com/rubenv/sql-migrate v0.0.0-20191213152630-06338513c237
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	github.com/uber/jaeger-client-go v2.15.0+incompatible
	gitlab.com/gitlab-org/gitlab-shell v0.0.0-20200821152636-82ec8144fb2a
	gitlab.com/gitlab-org/labkit v0.0.0-20200908084045-45895e129029
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	golang.org/x/sys v0.0.0-20200113162924-86b910548bc1
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/grpc v1.24.0
	gopkg.in/yaml.v2 v2.2.8
)

go 1.13
