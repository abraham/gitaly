Source: gitaly
Section: net
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>,
 Sruthi Chandran <srud@debian.org>
Build-Depends: debhelper (>= 11~),
 dh-golang (>= 1.43~),
 golang-any (>= 2:1.13~),
 gitlab-shell (>= 13.7~),
 golang-github-beorn7-perks-dev,
 golang-github-burntsushi-toml-dev,
 golang-github-client9-reopen-dev,
 golang-github-cloudflare-tableflip-dev (>= 1.2~),
 golang-github-davecgh-go-spew-dev,
 golang-github-dchest-safefile-dev,
 golang-goprotobuf-dev (>= 1.3~),
#~ golang-github-grpc-ecosystem-go-grpc-prometheus-dev,
 golang-github-google-uuid-dev,
 golang-github-kelseyhightower-envconfig-dev,
 golang-protobuf-extensions-dev,
#~ golang-github-getsentry-raven-go-dev,
 golang-github-lib-pq-dev (>= 1.3~),
 golang-github-pmezard-go-difflib-dev,
 golang-github-prometheus-client-golang-dev,
 golang-github-prometheus-client-model-dev,
 golang-github-prometheus-common-dev,
 golang-github-rubenv-sql-migrate-dev,
 golang-github-sebest-xff-dev,
 golang-github-sirupsen-logrus-dev (>= 1.0~),
 golang-github-stretchr-testify-dev (>= 1.4~),
 golang-golang-x-crypto-dev,
 golang-golang-x-net-dev,
 golang-golang-x-sync-dev,
 golang-golang-x-sys-dev,
 golang-golang-x-text-dev,
 golang-golang-x-tools,
 golang-google-grpc-dev (>= 1.22~),
#~ golang-google-genproto-dev,
 golang-honnef-go-tools-dev,
 golang-procfs-dev,
 golang-gopkg-gorp.v1-dev,
 golang-gopkg-libgit2-git2go.v28-dev,
 golang-gopkg-yaml.v2-dev,
 protobuf-compiler,
# Tests:
#  ,git
#  ,procps
 gem2deb,
 ruby-bundler,
 ruby-rugged (>= 0.28.1~),
 ruby-github-linguist (>= 7.9~),
 ruby-github-markup (>= 1.7.0~),
 ruby-activesupport (>= 2:6.0.3~),
 ruby-gollum-lib (>= 4.2.7.9-2~),
 ruby-gollum-rugged-adapter (>= 0.4.4.2~),
 ruby-grpc (>= 1.24~),
 ruby-grpc-tools (>= 1.24~),
 ruby-sentry-raven (>= 3.0~),
 ruby-faraday (>= 0.12~),
 ruby-rbtrace,
 ruby-gitlab-labkit (>= 0.12~),
 ruby-licensee (>= 8.9~),
 ruby-google-protobuf (>= 3.11.2~)
Standards-Version: 4.5.1
Homepage: https://gitlab.com/gitlab-org/gitaly
Vcs-Git: https://salsa.debian.org/go-team/packages/gitaly.git
Vcs-Browser: https://salsa.debian.org/go-team/packages/gitaly
XS-Ruby-Versions: all
XS-Go-Import-Path: gitlab.com/gitlab-org/gitaly

Package: gitaly
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${shlibs:Depends}, ${misc:Depends},
     gitlab-common (=${source:Version}),
     ruby | ruby-interpreter,
     ruby-bundler,
     ruby-rugged (>= 0.28.1~),
     ruby-github-linguist (>= 7.9~),
     ruby-github-markup (>= 1.7.0~),
     ruby-activesupport (>= 2:6.0.3~),
     ruby-gollum-lib (>= 4.2.7.9-2~),
     ruby-gollum-rugged-adapter (>= 0.4.4.2~),
     ruby-grpc (>= 1.24~),
     ruby-sentry-raven (>= 3.0~),
     ruby-faraday (>= 0.12~),
     ruby-rbtrace,
     ruby-gitlab-labkit (>= 0.12~),
     ruby-licensee (>= 8.9~),
     ruby-google-protobuf (>= 3.11.2~),
# sysv deps:
     pipexec,
     procps
Conflicts: libruby2.5
Description: Git RPC service for handling all the git calls made by GitLab
 Gitaly makes the git data storage tier of large GitLab instances fast.
 .
 This is achieved by moving git operations as close to the data as possible and
 Optimizing git services using caching.
 .
 Gitaly is a core service of gitlab.

Package: ruby-gitaly
Section: ruby
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-google-protobuf (>= 3.1~),
         ruby-grpc (>= 1.24~),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Auto-generated gRPC client for gitaly
 Auto-generated gRPC ruby client for gitaly.
 .
 Gitaly is part of GitLab. It is a server application that uses its own gRPC
 protocol to communicate with its clients.

Package: gitlab-common
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends},
 ruby | ruby-interpreter,
 adduser (>= 3.34~),
 git (>= 1:2.24~),
 ucf,
 gitlab-shell (>= 13.7~)
Description: git powered software platform to collaborate on code (common)
 gitlab provides web based interface to host source code and track issues.
 It allows anyone for fork a repository and send merge requests. Code review
 is possible using merge request workflow. Using groups and roles project
 access can be controlled.
 .
 This package includes configurations common to gitlab and gitaly.
