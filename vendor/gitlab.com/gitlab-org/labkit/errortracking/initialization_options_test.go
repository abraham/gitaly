package errortracking

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestWithLoggerName(t *testing.T) {
	loggerName := "test-logger"
	config := initializationConfig{}

	WithLoggerName(loggerName)(&config)

	require.Equal(t, config.loggerName, loggerName)
}
