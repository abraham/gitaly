#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${SCRIPT_DIR}"

for build_tags in \
  "" \
  "tracer_static tracer_static_jaeger tracer_static_lightstep tracer_static_datadog" \
  "continuous_profiler_stackdriver" \
  ; do
  (
    set -x;

    go test \
    -tags "${build_tags}" \
    ./...
  )
done
