package monitoring_test

import (
	"log"

	"gitlab.com/gitlab-org/labkit/monitoring"
)

func ExampleStart() {
	go func() {
		log.Fatal(monitoring.Start(
			// Listen on port 7822 on all interfaces
			monitoring.WithListenerAddress(":7822"),
			// Add the standard version and build time labels
			monitoring.WithBuildInformation("0.1.1", "2019-09-01T00:22:00Z"),
			// Add any additional application-specific labels to the `gitlab_build_info` metric
			monitoring.WithBuildExtraLabels(map[string]string{
				"git_version": "2.0.0",
			}),
		))
	}()
}
