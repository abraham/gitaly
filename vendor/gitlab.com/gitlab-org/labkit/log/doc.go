/*
Package log provides sensible logging defaults for Labkit.

Labkit uses Logrus for logging.

Applications that use Labkit should rely directly on Labkit.

This package provides some utility methods for working with Labkit, including:

1. Initialize Logrus in a consistent manner
1. Using GitLab's defaults with logrus
1. Passing correlation-ids between contexts and logrus

Please review the examples for more details of how to use this package.
*/
package log
