package errortracking

import (
	"github.com/getsentry/sentry-go"
)

// WithField allows to add a custom field to the error
func WithField(key string, value string) CaptureOption {
	return func(event *sentry.Event) {
		event.Tags[key] = value
	}
}
