package monitoring

// A generic profiler config holder. Avoid
// adding driver-specific data.
type profilerOpts struct {
	ServiceVersion string
}
