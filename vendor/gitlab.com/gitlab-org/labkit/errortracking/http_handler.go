package errortracking

import (
	"net/http"

	sentryhttp "github.com/getsentry/sentry-go/http"
)

// NewHandler will recover from panics inside handlers and reports the stacktrace to the errorreporting provider
func NewHandler(next http.Handler) http.Handler {
	sentryHandler := sentryhttp.New(sentryhttp.Options{
		Repanic:         false,
		WaitForDelivery: true,
	})

	return sentryHandler.HandleFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}
